﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerceptronUnicapaAvanzado
{
    class Program
    {
        static void Main(string[] args)
        {

            Neurona p = new Neurona(2,0.5f);

            //p.Pesos = new float[2];
            //p.Umbral = 0f;


            /*Todas las neuronas tiene un umbral, todas las neuronas tienen una carga inicial
             * y cada entrada le da una carga extra a la neurona
             * 
             * 
             * AND E1 E2
             * 1    1 1
             * 0    1 0
             * 0    0 1
             * 0    0 0
             */

            /* Error  = lo que necesiamos - lo que tenemos
             * Tasa de Aprendizaje = 0.3f
             * peso = pesoanterior + tasa de aprendizaje * error * entrada
             * 
             * 
             * 
             * 
             * 
             * 
             */

            //float[] pesosanteriores = new float[0];
            //float umbralanterior = -10;


            Random r = new Random();

            bool sw = false;

            while (!sw)
            {
                sw = true;
                //if (pesosanteriores.Length == 0)
                //{
                //    p.Pesos[0] = Convert.ToSingle(r.NextDouble() - r.NextDouble());
                //    p.Pesos[1] = Convert.ToSingle(r.NextDouble() - r.NextDouble());
                //    p.Umbral = Convert.ToSingle(r.NextDouble() - r.NextDouble());
                //    pesosanteriores = new float[2];
                //    pesosanteriores = p.Pesos;
                //    umbralanterior = p.Umbral;
                //}





                Console.Write("--------------------------\n");
                Console.Write("Peso 1 : " + p.Pesos[0] + "\n");
                Console.Write("Peso 1 : " + p.Pesos[1] + "\n");
                Console.Write("Umbral : " + p.Umbral + "\n");
                Console.Write("E1:1 E2:1 : " + p.Salida(new float[2] { 1f, 1f }) + "\n");
                Console.Write("E1:1 E2:0 : " + p.Salida(new float[2] { 1f, 0f }) + "\n");
                Console.Write("E1:0 E2:1 : " + p.Salida(new float[2] { 0f, 1f }) + "\n");
                Console.Write("E1:0 E2:0 : " + p.Salida(new float[2] { 0f, 0f }) + "\n");

                if (p.Salida(new float[2] { 1f, 1f }) != 1)
                {

                    //if (pesosanteriores.Length != 0)
                    //{
                    //    p.Pesos[0] = pesosanteriores[0] + 0.3f * (1 - p.Salida(new float[2] { 1f, 1f })) * 1;
                    //    p.Pesos[1] = pesosanteriores[1] + 0.3f * (1 - p.Salida(new float[2] { 1f, 1f })) * 1;
                    //    p.Umbral = umbralanterior + 0.3f * (1 - p.Salida(new float[2] { 1f, 1f }));
                    //}
                    p.Aprender(new float[2] { 1f, 1f },1);
                    sw = false;
                }

                if (p.Salida(new float[2] { 1f, 0f }) != 0)
                {
                    //if (pesosanteriores.Length != 0)
                    //{
                    //    p.Pesos[0] = pesosanteriores[0] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 1;
                    //    p.Pesos[1] = pesosanteriores[1] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 0;
                    //    p.Umbral = umbralanterior + 0.3f * (1 - p.Salida(new float[2] { 1f, 1f }));
                    //}
                    p.Aprender(new float[2] { 1f, 0f }, 0);
                    sw = false;
                }


                if (p.Salida(new float[2] { 0f, 1f }) != 0)
                {
                    //if (pesosanteriores.Length != 0)
                    //{
                    //    p.Pesos[0] = pesosanteriores[0] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 0;
                    //    p.Pesos[1] = pesosanteriores[1] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 1;
                    //    p.Umbral = umbralanterior + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f }));
                    //}
                    p.Aprender(new float[2] { 0f, 1f }, 0);
                    sw = false;
                }

                if (p.Salida(new float[2] { 0f, 0f }) != 0)
                {
                    //if (pesosanteriores.Length != 0)
                    //{
                    //    p.Pesos[0] = pesosanteriores[0] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 0;
                    //    p.Pesos[1] = pesosanteriores[1] + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f })) * 0;
                    //    p.Umbral = umbralanterior + 0.3f * (0 - p.Salida(new float[2] { 1f, 1f }));
                    //}
                    p.Aprender(new float[2] { 0f, 0f }, 0);
                    sw = false;
                }
                //pesosanteriores = p.Pesos;
                //umbralanterior = p.Umbral;


            }

            Console.ReadKey();





        }


    }

    public class Neurona
    {
        float[] PesosAnteriores;
        float UmbralAnterior;
        public float[] Pesos;
        public float Umbral;
        public float TasaDeAprendizaje = 0.3f;

        public Neurona(int NEntradas, float tasaAprendizaje2 = 0.3f)
        {
            TasaDeAprendizaje = tasaAprendizaje2;
            Pesos = new float[NEntradas];
            PesosAnteriores = new float[NEntradas];
            Aprender();

        }
       

        public void Aprender()
        {
            
            Random r = new Random();
            for (int i = 0;i <PesosAnteriores.Length;i++)
            {
            PesosAnteriores[i] = Convert.ToSingle(r.NextDouble() - r.NextDouble()); ;
            }
               
            UmbralAnterior = Convert.ToSingle(r.NextDouble() - r.NextDouble()); ;
            Pesos =PesosAnteriores ;
            Umbral= UmbralAnterior  ;
          

        }

        public void Aprender(float[] entradas, float salidaEsperada)
        {
            if (PesosAnteriores != null)
            {
                float error = salidaEsperada - Salida(entradas);
                for (int i = 0; i < Pesos.Length; i++)
                {
                    Pesos[i] = PesosAnteriores[i] + TasaDeAprendizaje * error * entradas[i];

                }
                Umbral = UmbralAnterior + TasaDeAprendizaje * error;
                PesosAnteriores = Pesos;
                UmbralAnterior = Umbral;

            }
            else
            {
                Random r = new Random();
            for (int i = 0; i < PesosAnteriores.Length; i++)
            {
                PesosAnteriores[i] = Convert.ToSingle(r.NextDouble() - r.NextDouble()); ;
            }

            UmbralAnterior = Convert.ToSingle(r.NextDouble() - r.NextDouble()); ;
                Pesos = PesosAnteriores;
                Umbral = UmbralAnterior;
            }

    }

        public float Salida(float[] entradas)
        {
            return Sigmoid(neurona(entradas));
        }
        float neurona(float[] entradas)
        {
            float sum = 0;
            for (int i = 0; i < Pesos.Length; i++)
            {
                sum += entradas[i] * Pesos[i];
            }
            sum += Umbral;

            return sum;

        }
        float Sigmoid(float d)
        {
            return d > 0 ? 1 : 0;

        }
    }

}



